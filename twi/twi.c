/** 
 *
 *
 *
 *@author Mantas Jurkuvenas <Mantasjurkuvenas@gmail.com>
 *
 */

#include "twi.h"
#include "sdk_macros.h"
#include <stdio.h>
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

ret_code_t twi_read(nrf_drv_twi_t *p_twi_instance, uint8_t i2c_address, uint8_t reg, uint8_t *p_data, uint8_t data_length)
{
    // ret_code_t err_code;

    // err_code = nrfx_twi_tx(p_twi_instance, i2c_address, &reg, sizeof(reg), true);
    // VERIFY_SUCCESS(err_code);

    // err_code = nrfx_twi_rx(p_twi_instance, i2c_address, p_data, data_length);
    // VERIFY_SUCCESS(err_code);

     return NRF_SUCCESS;
}

ret_code_t twi_write(nrf_drv_twi_t *p_twi_instance, uint8_t i2c_address, uint8_t reg, uint8_t *data, uint8_t data_length)
{
    // ret_code_t err_code;
    // uint8_t command[20];
    // command[0] = reg;
    // memcpy(&command[1], data, data_length); // copy data to internal buffer

    // err_code = nrfx_twi_tx(p_twi_instance, i2c_address, command, data_length + 1, false);

    return NRF_SUCCESS;
}

ret_code_t twi_scan(nrf_drv_twi_t *p_twi_instance, uint8_t *data, uint8_t data_length)
{
    uint8_t sample_data;
    data_length = 0;
    ret_code_t err_code;
    for (uint8_t address = 1; address <= 127; address++)
    {
        err_code = nrf_drv_twi_rx(p_twi_instance, address, &sample_data, sizeof(sample_data));
        if (err_code == NRF_SUCCESS)
        {
            NRF_LOG_INFO("TWI device detected at address 0x%x.", address);
            //add address to the array if found
            data[data_length] = address;
            data_length++;
        }
        NRF_LOG_FLUSH();
    }
    return NRF_SUCCESS;
}
/** 
 *
 *
 *
 *@author Mantas Jurkuvenas <Mantasjurkuvenas@gmail.com>
 *
 */

#ifndef TWI_H
#define TWI_H

#include "nrfx_twi.h"
#include "sdk_errors.h"
#include "nrf_drv_twi.h"


/**
 * @brief       Function for disabling TWI instance.
 * 
 * @param[in]   p_twi_instance Pointer to the driver instance structure.
 */
void twi_disable(nrfx_twi_t *p_twi_instance);

/**
 * @brief I2C read register
 * 
 * @param p_i2c_instance pointer to i2c instance
 * @param i2c_address i2c address to read
 * @param reg reagister to read
 * @param p_data pointer where to save data
 * @param data_length length of data to read
 * @return NRF_SUCCESS on success. Otherwise, an error code is returned.
 */
ret_code_t twi_read(nrf_drv_twi_t *p_twi_instance, uint8_t i2c_address, uint8_t reg, uint8_t *p_data, uint8_t data_length);

/**
 * @brief I2c Write Register
 * 
 * @param p_i2c_instance pointer to i2c instance
 * @param i2c_address i2c address to writre
 * @param reg reagister to write
 * @param data pointer where to save data
 * @param data_length length of data to write
 * @return ret_code_t  NRF_SUCCESS on success. Otherwise, an error code is returned.
 */
ret_code_t twi_write(nrf_drv_twi_t *p_twi_instance, uint8_t i2c_address, uint8_t reg, uint8_t *data, uint8_t data_length);

/**
 * @brief Scan for I2C devices
 * 
 * @param p_twi_instance pointer to i2c instance
 * @param data pointer to store i2c addresses of found devices
 * @param data_length amount of devices found
 * @return ret_code_t 
 */
ret_code_t twi_scan(nrf_drv_twi_t *p_twi_instance, uint8_t *data, uint8_t data_length);
#endif //
